import Dependencies._

lazy val root = (project in file(".")).
  settings(
    inThisBuild(List(
      organization := "scaffold.easy-pg.net",
      scalaVersion := "2.12.6",
      version      := "0.1.0-SNAPSHOT"
    )),
    name := "scaffold-code-generator",
    libraryDependencies += scalaTest % Test
  )
